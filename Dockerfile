ARG PYTHON_VERSION=3.7-slim

FROM python:${PYTHON_VERSION} AS base
ARG RUN_DEPS="libpcre3 libpq5 mime-support"
ARG BUILD_DEPS="build-essential libpcre3-dev libpq-dev"
ENV PYTHONUNBUFFERED 1
ENV RUN_DEPS=${RUN_DEPS}
ENV BUILD_DEPS=${BUILD_DEPS}
# Install packages needed to run your application (not build deps):
#   mime-support -- for mime types when serving static files
# We need to recreate the /usr/share/man/man{1..8} directories first because
# they were clobbered by a parent image.
RUN set -ex \
    && apt-get update \
    && apt-get install -y --no-install-recommends ${RUN_DEPS} \
    && rm -rf /var/lib/apt/lists/*

# Builder that holds build deps
FROM base AS builder
RUN set -ex \
    && apt-get update && apt-get install -y --no-install-recommends ${BUILD_DEPS} \
    && rm -rf /var/lib/apt/lists/*
WORKDIR /wheels
COPY setup.py setup.cfg manage.py requirements.txt /wheels/
RUN pip install -U pip &&  pip wheel -r ./requirements.txt

# Target that holds built eggs and app
FROM base AS stripped
# Install build deps, then run `pip install`, then remove unneeded build deps all in a single step.
# Correct the path to your production requirements file, if needed.
COPY --from=builder /wheels /wheels
RUN set -ex \
    && cd /wheels \
    && pip install -U pip \
    && pip install -r /wheels/requirements.txt -f /wheels \
    && rm -rf /wheels \
    && rm -rf /root/.cache/pip/* \
    && rm -rf \
      /usr/local/bin/idle* \
      /usr/local/lib/python3/idlelib \
      /usr/local/lib/python3/tkinter \
      /usr/local/lib/python3/turtle.py \
      /usr/local/lib/python3/turtledemo \
      /usr/local/lib/python3/test \
      /usr/local/lib/python3/*/test \
      /usr/local/lib/python3/*/tests

# Add run user
RUN groupadd django \
    && useradd -g django django

# Define entry point and command
ENTRYPOINT ["/entrypoint"]
CMD ["uwsgi", "--show-config", "/run/secrets/app.config.ini"]

# uWSGI will listen on this port
EXPOSE 8000

COPY docker-entrypoint.py /entrypoint
RUN sed -i 's/\r//' /entrypoint
RUN chmod +x /entrypoint
RUN chown django /entrypoint

# Add Application logic from here on
FROM stripped AS target

WORKDIR /
ADD ./app/ /app/
COPY manage.py /
RUN chown -R django /app/

# Call collectstatic (customize the following line with the minimal environment variables needed for manage.py to run):
RUN mkdir -p /run/secrets/ \
	&& echo "[django]\nsecret=none" > /run/secrets/app.secret.ini \
	&& DATABASE_URL='' python manage.py collectstatic --noinput \
	&& rm /run/secrets/app.secret.ini
