#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""Docker entrypoint that does not need a shell."""

import os
import sys
import importlib
import subprocess
import psycopg2
from time import sleep
from app.config import get_config


def postgres_ready(pg_user, pg_password, pg_host, pg_port, pg_db):
    """Test if a connection to postgreSQL succeeds."""
    try:
        psycopg2.connect(
            dbname=pg_db,
            user=pg_user,
            password=pg_password,
            host=pg_host,
            port=pg_port
        )
    except psycopg2.OperationalError as exc:
        sys.stderr.write(str(exc))
        return False
    return True


def wait_for_postgres(config):
    """Wait at most 30 seconds for postgreSQL to become ready."""
    max_wait = 30
    while 0 < max_wait and not postgres_ready(
        config['user'], config['password'],
        config['host'], config['port'],
        config['db']
    ):
        sys.stderr.write(
            'Waiting for PostgreSQL to become available...')
        sleep(1)
        max_wait = max_wait - 1
    if 0 >= max_wait:
        sys.exit(1)


def django_migrate(config):
    """Perform migrations when env setting says so."""
    if not config['migrate']:
        return

    os.environ['DJANGO_SETTINGS_MODULE'] = config['settings']
    sys.path.append(os.getcwd())

    try:
        from django.core.management import execute_from_command_line
    except ImportError as exc:
        raise ImportError(
            "Couldn't import Django. Are you sure it's installed and "
            "available on your PYTHONPATH environment variable? Did you "
            "forget to activate a virtual environment?"
        ) from exc

    execute_from_command_line(['manage.py', 'migrate', '--noinput'])


def main():
    """Docker entry point logic for django + postgreSQL."""
    config = get_config()
    wait_for_postgres(config['postgres'])
    django_migrate(config['django'])
    subprocess.call(sys.argv[1:])


if __name__ == '__main__':
    main()
