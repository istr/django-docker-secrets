#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""Configuration from docker secret compatible files."""

import configparser
from os import environ as env
from typing import Dict, Any
import sys


def get_config():
    """Fetch config from docker secret compatible files."""
    config = configparser.ConfigParser()
    secret = configparser.ConfigParser()

    config.read('/run/secrets/app.config.ini')
    secret.read('/run/secrets/app.secret.ini')

    config_set: Dict[str, Any] = dict()
    if config.has_section('postgres'):
        cfg_pg = config['postgres']
        config_set['postgres'] = {
            'user': cfg_pg.get('user', 'postgres'),
            'host': cfg_pg.get('host', 'localhost'),
            'port': cfg_pg.getint('port', fallback=5432),
            'db': cfg_pg.get('db'),
            'password': secret.get('postgres', 'password')
        }

    if config.has_section('django'):
        cfg_django = config['django']
        config_set['django'] = {
            'migrate': cfg_django.getboolean('migrate_on_start', fallback=True),
            'settings': cfg_django.get('settings_module', 'app.settings'),
            'debug': cfg_django.getboolean('debug', fallback=False),
            'allowed_hosts': cfg_django.get('allowed_hosts', 'localhost'),
        }
    else:
        config_set['django'] = {}

    config_set['django']['secret'] = secret.get('django', 'secret')
    return config_set
