#!/bin/sh
# set up a docker user and db

set -e

CFG_INI=${1:-/run/secrets/app.config.ini}
SEC_INI=${2:-/run/secrets/app.secret.ini}

parse_line() {
  sed -nr "/^\[${2}\]/ { :l /${3}[ ]*=/ { s/.*=[ ]*//; p; q;}; n; b l;}" < "${1}"
}

POSTGRES_DB=$(parse_line "${CFG_INI}" 'postgres' 'db')
POSTGRES_USER=$(parse_line "${CFG_INI}" 'postgres' 'user')
POSTGRES_PASSWORD=$(parse_line "${SEC_INI}" 'postgres' 'password')

echo "Creating DB ${POSTGRES_DB} for USER ${POSTGRES_USER}" >&2
psql <<- EOSQL
    CREATE USER ${POSTGRES_USER} WITH PASSWORD '${POSTGRES_PASSWORD}';
    CREATE DATABASE ${POSTGRES_DB};
    GRANT ALL PRIVILEGES ON DATABASE ${POSTGRES_DB} TO ${POSTGRES_USER};
EOSQL
echo "Done Creating DB" >&2
